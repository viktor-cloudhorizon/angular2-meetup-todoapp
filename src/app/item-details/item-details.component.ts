import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { ItemsService } from "../items.service";

import { Item } from "../../models/item";

@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.css']
})
export class ItemDetailsComponent implements OnInit {
  @Input() item: Item;

  @Output() notifyChange: EventEmitter<Item> = new EventEmitter<Item>();

  constructor(private itemsService: ItemsService) { }

  ngOnInit() { }

  completeItem(item: Item) {
    this.itemsService.setComplete(item.id, item.completed).then((data) => {
      this.notifyChange.emit(item);
    });
  }

}
