import { Component, OnInit } from '@angular/core';

import { ItemDetailsComponent } from '../item-details/item-details.component';
import { ItemListComponent } from '../item-list/item-list.component';

import { ItemsService } from "../items.service";

import { Item } from "../../models/item";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  private items: Item[];
  private selectedItem: Item;

  constructor(private itemsService: ItemsService) {

  }

  ngOnInit() {
    this.loadItems();
  }

  loadItems() {
    this.itemsService.getAllItems().then((data) => {
      this.items = data;
      if (!this.selectedItem && this.items && this.items.length > 0) {
        this.selectedItem = this.items[0];
      }
    });
  }

  selectItem(item: Item) {
    if (item) {
      this.selectedItem = item;
    }
  }

  itemChanged(item: Item) {
    this.loadItems();
  }
}
