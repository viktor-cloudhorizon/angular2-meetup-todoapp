import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { Item } from "../models/item";

@Injectable()
export class ItemsService {
  items = [
    new Item(1, 'Buy bread', 'Make sure it is fresh. I mean it. Do it.'),
    new Item(2, 'Buy milk', 'Check expiry date first. You do not want to get sick again.'),
    new Item(3, 'Call John', 'Yes, I know. Uncreative name. Mister John Smith generic.')
  ];

  constructor() { }

  getAllItems(): Promise<Item[]> {
    return new Promise((resolve, reject) => {
      this.items[0].completed = true;

      resolve(this.items);
    });
  }

  add(item: Item): Promise<Item> {
    return new Promise((resolve, reject) => {
      if (item && item.title && item.description) {
        item.id = this.getNewItemId();
        this.items.push(item);

        resolve(item);
      } else {
        reject("Validation failed!");
      }
    });
  }

  setComplete(id: number, complete: boolean) {
    return new Promise((resolve, reject) => {
      const items = this.items.filter((item) => item.id == id);
      if (items.length != 1) reject();
      const item = items[0];
      item.completed = complete;

      resolve(item);
    });
  }

  private getNewItemId(): number {
    let id = 0;
    for (let i = 0; i < this.items.length; i++) {
      var element = this.items[i];
      if (element.id > id) {
        id = element.id;
      }
    }
    return id + 1;
  }

}
