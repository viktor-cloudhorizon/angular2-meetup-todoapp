import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Item } from '../../models/item';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {
  @Input() items: Item[];

  @Output() notifySelectedItem: EventEmitter<Item> = new EventEmitter<Item>();

  constructor() { }

  ngOnInit() {
  }

  selectItem(item: Item) {
    this.notifySelectedItem.emit(item);
  }
}
