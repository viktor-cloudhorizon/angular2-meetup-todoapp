import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ItemsService } from "../items.service";

import { Item } from "../../models/item";

@Component({
  selector: 'app-item-editor',
  templateUrl: './item-editor.component.html',
  styleUrls: ['./item-editor.component.css']
})
export class ItemEditorComponent implements OnInit {
  title: string;
  description: string;

  constructor(private router: Router, private itemsService: ItemsService) { }

  ngOnInit() {
  }

  save() {
    this.itemsService.add(new Item(0, this.title, this.description)).then((data) => {
      this.router.navigate(['/']);
    });
  }

}
