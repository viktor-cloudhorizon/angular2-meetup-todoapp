export class Item {
  id: number;
  title: string;
  description: string;
  completed: boolean;

  constructor(i: number, t: string, d: string) {
    this.id = i;
    this.title = t;
    this.description = d;
    this.completed = false;
  }
}
